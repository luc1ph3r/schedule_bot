var TelegramBot   = require('node-telegram-bot-api'),
    fs            = require('fs'),
    Promise       = require('bluebird'),
    request       = Promise.promisifyAll(require('request'), {suffix: 'Async'}),
    uuid          = require('node-uuid'),
    miscellaneous = require('./miscellaneous.js');

const REPLY_KB = {
    keyboard: [
        ['Пары на сегодня', 'Пары на завтра'],
        ['Пары на неделю']
    ],
    resize_keyboard:   true,
    one_time_keyboard: true
};

const REPLY_KB_WEEK = {
    keyboard: [
        ['Понедельник', 'Вторник'],
        ['Среда',       'Четверг'],
        ['Пятница',     'Суббота'],
        ['Назад']
    ],
    resize_keyboard:   true,
    one_time_keyboard: true,
    selective:         true
};

const INFO_MESSAGES = {
    unexpectedError:   'Произошла ошибка! ):',
    alreadyExisted:    'Ты уже есть в базе ¯\\_(ツ)_/¯',
    zeroLessons:       'Пар нет (;',
    oldMenu:           'Старое меню:',
    whichDayQuestion:  'Какой день?',
    createAccountInfo: 'Сначала надо создать аккаунт!\nНабери `/start`',
    accountDeleted:    'Аккаунт был удалён из базы данных 👌',

    succesfulyCreated: 'Отлично! Ты добавлен в базу.\n\n' +
                       'Теперь ты можешь смотреть расписание:',

    newUser:           'Привет! Тебя нет в базе. Чтобы отправлять тебе ' +
                       'расписание, мне нужно знать твой email.' +
                       '\n\nДля этого отправь сообщение' +
                       '\n`/account <твой email>@edu.hse.ru`'
};

module.exports = class ScheduleBot {
    constructor(config) {
        if (! (config && config.token && config.options)) {
            throw new Error('Wrong config for ScheduleBot');
        }

        this.core = new TelegramBot(config.token, config.options);
    }

    init() {
        this.core.on('message', (msg) => {
            this.regularMessage(this, msg);
        });
        this.core.onText(/\/account [a-z]*@edu.hse.ru/, (msg) => {
            this.createAccount(this, msg);
        });

        return Promise.resolve();
    }

    sendNewUserMsg(chatId) {
        return this.core.sendMessage(
            chatId,
            INFO_MESSAGES.newUser,
            {parse_mode: 'markdown'}
        );
    }

    sendErrorMsg(chatId) {
        return this.core.sendMessage(chatId, INFO_MESSAGES.unexpectedError);
    }

    sendOldMenu(chatId) {
        this.core.sendMessage(
            chatId,
            INFO_MESSAGES.oldMenu,
            {reply_markup: REPLY_KB}
        );
    }

    sendWhichDayQuestion(chatId) {
        this.core.sendMessage(
            chatId,
            INFO_MESSAGES.whichDayQuestion,
            {reply_markup: REPLY_KB_WEEK}
        );
    }

    getSchedule(chatId, username, input, extras) {
        let ids = {
            chatId,
            username,
            component: 'getSchedule'
        };

        if (extras && extras.request && extras.request.id) {
            ids.request = extras.request;
        } else {
            ids.request = {
                id: uuid.v4().replace(/-/g, '')
            };
        }

        let subLogger = this.logger.child(ids);

        this.dbClient.findUser(username)
        .then(results => {
            if (0 === results.length) {
                subLogger.warn(`Not found user in DB: ${username}`);

                return this.sendNewUserMsg(chatId);
            }

            let email = results[0].email;
            let url   = miscellaneous.constructUrl(email, input);

            subLogger.info(`Getting info from url: ${url}`);

            return request.getAsync(url)
            .then((response) => {
                subLogger.info('Got info from url');

                let parseResult = this.parseSchedule(response.body, {request: ids.request});
                this.core.sendMessage(chatId, parseResult);

                subLogger.info('Responded to request');
            });
        })
        .catch(err => {
            subLogger.error(
                `Got error while getting response: ${JSON.stringify(err)}`
            );

            this.sendErrorMsg(chatId);
        });
    }

    checkCommand(chatId, username, msg, extras) {
        let ids = {
            chatId,
            username,
            component: 'checkCommand'
        };

        if (extras && extras.request && extras.request.id) {
            ids.request = extras.request;
        } else {
            ids.request = {
                id: uuid.v4().replace(/-/g, '')
            };
        }

        let subLogger = this.logger.child(ids);

        let specificDate = /[0-9]{1,2}.[0-9]{1,2}.201[0-9]/.exec(msg);

        if (specificDate) {
            subLogger.info(`Got request for specific date: ${specificDate}`);

            specificDate = specificDate[0];

            return this.getSchedule(chatId, username, {specificDate});
        }

        switch (msg.toLowerCase()) {
            case 'назад':
                this.sendOldMenu(chatId);
                break;
            case 'пары на сегодня':
            case 'сегодня':
                this.getSchedule(chatId, username, miscellaneous.getDayOfWeek());
                break;
            case 'пары на завтра':
            case 'завтра':
                this.getSchedule(chatId, username, miscellaneous.getDayOfWeek(1));
                break;
            case 'пары на неделю':
            case 'неделя':
                this.sendWhichDayQuestion(chatId);
                break;
            case 'понедельник':
                this.getSchedule(chatId, username, 1);
                break;
            case 'вторник':
                this.getSchedule(chatId, username, 2);
                break;
            case 'среда':
                this.getSchedule(chatId, username, 3);
                break;
            case 'четверг':
                this.getSchedule(chatId, username, 4);
                break;
            case 'пятница':
                this.getSchedule(chatId, username, 5);
                break;
            case 'суббота':
                this.getSchedule(chatId, username, 6);
                break;
            default:
                subLogger.warn({msg}, 'Unknown request type');
                break;
        }
    }

    checkUser(chatId, username, extras) {
        let ids = {
            chatId,
            username,
            component: 'checkUser'
        };

        if (extras && extras.request && extras.request.id) {
            ids.request = extras.request;
        } else {
            ids.request = {
                id: uuid.v4().replace(/-/g, '')
            };
        }

        let subLogger = this.logger.child(ids);
        subLogger.info(`Checking if user exists`);

        return this.dbClient.findRecords({username: username}, 'users')
        .then(results => {
            if (0 === results.length) {
                return Promise.reject(INFO_MESSAGES.createAccountInfo);
            }

            subLogger.info('Found user');
            return Promise.resolve();
        })
        .catch(err => {
            if (err === INFO_MESSAGES.createAccountInfo) {
                return Promise.reject(err);
            }

            this.sendErrorMsg(chatId);

            subLogger.error(`DB Error: ${JSON.stringify(err)}`);

            return Promise.reject();
        });
    }

    regularMessage(self, msg) {
        let username = msg.chat.username;
        let chatId   = msg.chat.id;

        let ids = {
            chatId,
            username,
            request: {
                id: uuid.v4().replace(/-/g, '')
            },
            component: 'regularMessage'
        };

        let subLogger;

        Promise.bind(self)
        .then(function() {
            subLogger = this.logger.child(ids);
            subLogger.info(`Got message from user: ${msg.text}`);

            if (msg.text != '/start' && msg.text != '/delete' &&
                !msg.text.includes('/account')) {
                this.checkUser(chatId, username, {request: ids.request})
                .then(() => {
                    this.checkCommand(chatId, username, msg.text);
                })
                .catch(msg => {
                    this.core.sendMessage(chatId, msg, {parse_mode: 'markdown'});
                });
            } else {
                if (msg.text == '/start') {
                    this.dbClient.findUser(username)
                    .then(() => {
                        this.core.sendMessage(
                            chatId,
                            'Here you go',
                            {reply_markup: REPLY_KB}
                        );
                    })
                    .catch(err => {
                        this.sendNewUserMsg(chatId);
                    });
                }

                if (msg.text == '/delete') {
                    this.dbClient.removeUser(username)
                    .then(() => {
                        subLogger.info(`Succesfuly deleted user`);

                        this.core.sendMessage(chatId, INFO_MESSAGES.accountDeleted);
                    })
                    .catch(err => {
                        this.core.sendMessage(chatId, INFO_MESSAGES.unexpectedError);

                        subLogger.error(
                            `Got error while deleting user: ${JSON.stringify(err)}`
                        );
                    });
                }
            }
        });
    }

    createAccount(self, msg) {
        let chatId   = msg.chat.id;
        let username = msg.chat.username;

        let ids = {
            chatId,
            username,
            request: {
                id: uuid.v4().replace(/-/g, '')
            },
            component: 'createAccount'
        };

        let subLogger;

        Promise.bind(self)
        .then(function() {
            subLogger = this.logger.child(ids);
            subLogger.info('Creating new user');

            let tmp   = msg.text.split(' ');
            let email = tmp[1];

            return this.dbClient.findRecords({username: username}, 'users')
            .then(results => {
                if (results.length > 0) {
                    return this.core.sendMessage(
                        chatId,
                        INFO_MESSAGES.alreadyExisted,
                        {reply_markup: REPLY_KB}
                    );
                }

                return this.dbClient.addUser({
                    username: username,
                    email:    email,
                    request:  ids.request
                })
                .then(() => this.core.sendMessage(
                    chatId,
                    INFO_MESSAGES.succesfulyCreated,
                    {reply_markup: REPLY_KB}
                ));
            });
        })
        .catch(err => {
            this.sendErrorMsg(chatId);

            return subLogger.warn('ошибка поиска пользователя в базе');
        });
    }

    parseOneDay(schedule) {
        if (schedule === undefined || schedule === null || schedule.length === 0) {
            return INFO_MESSAGES.zeroLessons;
        }

        let today = '';

        for (let i = 0; i != schedule.length; ++i) {
            let lesson  = schedule[i];
            let begin   = lesson.beginLesson;
            let end     = lesson.endLesson;
            let name    = lesson.discipline.slice(0, -5).trim();
            let type    = lesson.kindOfWork;
            let teacher = lesson.lecturer;
            let room    = lesson.auditorium;

            today += `${begin} - ${end}\n${name} (${type})\n${teacher}\n` +
                     `${room}\n~~~~~~~~~~~~~~~~\n`;
        }

        return today;
    }

    parseSchedule(schedule, extras) {
        let ids = {
            component: 'parseSchedule'
        };

        if (extras && extras.request && extras.request.id) {
            ids.request = extras.request;
        } else {
            ids.request = {
                id: uuid.v4().replace(/-/g, '')
            };
        }

        let subLogger = this.logger.child(ids);

        try {
            schedule = JSON.parse(schedule);
        } catch(err) {
            subLogger.error('Error while parsing schedule');

            return INFO_MESSAGES.unexpectedError;
        }

        if (schedule.length === 0) {
            return INFO_MESSAGES.zeroLessons;
        }

        return this.parseOneDay(schedule);
    }
};
