var MongoClient = require('mongodb').MongoClient,
    uuid        = require('node-uuid');

module.exports = class DBClient {
    constructor(config) {
        if (! (config)) {
            throw new Error('Wrong config for DBClient');
        }

        if (config.auth) {
            if (! (config.user && config.password)) {
                throw new Error('Missing db user or password');
            }

            this.user       = config.user;
            this.password   = config.password;
        }

        this.port       = config.port;
        this.host       = config.host;
        this.collection = config.collection;
    }

    init() {
        let url;

        if (this.user) {
            url = `mongodb://${this.user}:${this.password}` +
                  `@${this.host}:${this.port}/${this.collection}`;
        } else {
            url = `mongodb://${this.host}:${this.port}/${this.collection}`;
        }

        return new Promise((resolve, reject) => {
            MongoClient.connect(url, (error, database) => {
                if (error) {
                    throw new Error("error with connection to database");
                }

                this.db = database;

                return resolve();
            });
        });
    }

    addRecord(record, collectionName, extras) {
        let ids = {
            record,
            collectionName,
            component: 'addRecord'
        };

        if (extras && extras.request && extras.request.id) {
            ids.request = extras.request;
        } else {
            ids.request = {
                id: uuid.v4().replace(/-/g, '')
            };
        }

        let subLogger = this.logger.child(ids);

        return new Promise((resolve, reject) => {
            let collection = this.db.collection(collectionName);

            collection.insert(record, (error, result) => {
                if (error) {
                    throw error;
                }

                subLogger.info(`inserted record to ${collectionName}`);

                return resolve();
            });
        });
    }

    addUser(user, extras) {
        return new Promise((resolve, reject) => {
            this.addRecord(user, 'users', extras)
            .then(() => {
                return resolve();
            })
            .catch(function() {
                return reject();
            });
        });
    }

    deleteRecord(record, collectionName) {
        let collection = this.db.collection(collectionName);

        collection.remove(record, (error, result) => {
            if (error) {
                throw error;
            }
        });
    }

    findRecords(record, collectionName) {
        return new Promise((resolve, reject) => {
            let collection = this.db.collection(collectionName);

            collection.find(record).toArray((error, result) => {
                if (error) {
                    throw error;
                }

                return resolve(result);
            });
        });
    }

    findUser(username) {
        return new Promise((resolve, reject) => {
            this.findRecords({username: username}, 'users')
            .then((arr) => {
                if (arr.length > 0) {
                    return resolve(arr);
                } else {
                    return reject();
                }
            });
        });
    }

    removeUser(username) {
        return new Promise((resolve, reject) => {
            let collection = this.db.collection('users');

            collection.remove({username: username}, (error, result) => {
                if (error) {
                    return reject(error);
                } else {
                    return resolve();
                }
            });
        });
    }
};