const templateUrl = 'http://ruz.hse.ru/RUZService.svc/personlessons?language=1&groupoid=3907&';
const timeOffSet  =  7 * 3600 * 1000;

function constructUrl(email, inputDay) {
    let url = templateUrl + `email=${email}&`;
    let dateStr;

    if (inputDay.specificDate) {
        let [day, month, year] = inputDay.specificDate.split('.');

        month = month.replace(/^0/, '');
        day   = day.replace(/^0/, '');

        dateStr = `${year}.${month}.${day}`;
    } else {
        dateStr = parseDate(getDay(inputDay));
    }

    url += `fromdate=${dateStr}&`;
    url += `todate=${dateStr}`;

    return url;
}

function getDay(dayOfWeek) {
    dayOfWeek = parseInt(dayOfWeek);

    if (0 === dayOfWeek) {
        dayOfWeek = 7;
    }

    let today = new Date(Date.now() + timeOffSet);
    let diff  = dayOfWeek - today.getDay();

    if (diff < 0) {
        diff += 7;
    }

    return (new Date(today.getTime() + (diff) * 24 * 3600 * 1000));
}

function getDayOfWeek(dayOffset) {
    let theDay = new Date(Date.now() + timeOffSet);

    if (dayOffset) {
        theDay = new Date(theDay.getTime() + (dayOffset) * 24 * 3600 * 1000);
    }

    return theDay.getDay();
}

function parseDate(date) {
    let month  = date.getMonth() + 1;
    let day    = date.getDate();
    let year   = date.getFullYear();

    return `${year}.${month}.${day}`;
}

module.exports = {
    constructUrl: constructUrl,
    getDay:       getDay,
    getDayOfWeek: getDayOfWeek,
    parseDate:    parseDate
};
