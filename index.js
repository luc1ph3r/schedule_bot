var fs          = require('fs'),
    Promise     = require('bluebird'),
    bunyan      = require('bunyan'),
    toml        = require('toml'),
    ScheduleBot = require('./src/ScheduleBot'),
    DBClient    = require('./src/DBClient'),
    config      = toml.parse( fs.readFileSync(process.argv[2] || './config.toml') );

// ---- CREATING CLIENTS ------

let scheduleBot = new ScheduleBot(config.scheduleBot);
let dbClient    = new DBClient(config.mongoClient);
let logger      = new bunyan.createLogger({name: 'ScheduleBot', level: 10});

// ---- DEPENDENCY INJECTION --

scheduleBot.logger   = dbClient.logger = logger;
scheduleBot.dbClient = dbClient;

// ---- INITIALIZAITON --------

Promise.join([dbClient.init(), scheduleBot.init()])
.then(() => {
    logger.info('Started!');
})
.catch(err => {
    throw new Error('Failed to initialize');
});
