## Schedule Bot

Телеграм бот для получения расписания из РУЗа (посредством API).

<!-- TOC depthFrom:1 depthTo:6 withLinks:1 updateOnSave:1 orderedList:0 -->

	- [Schedule Bot](#schedule-bot)
		- [Установка](#%D0%A3%D1%81%D1%82%D0%B0%D0%BD%D0%BE%D0%B2%D0%BA%D0%B0)
		- [Конфигурация](#%D0%9A%D0%BE%D0%BD%D1%84%D0%B8%D0%B3%D1%83%D1%80%D0%B0%D1%86%D0%B8%D1%8F)
		- [Запуск](#%D0%97%D0%B0%D0%BF%D1%83%D1%81%D0%BA)
		- [Скриншоты](#%D0%A1%D0%BA%D1%80%D0%B8%D0%BD%D1%88%D0%BE%D1%82%D1%8B)

<!-- /TOC -->

### Установка

Требуемая версия NodeJS: **6.3.0+**

Установить все зависимости:

```shell
$ npm install
```

### Конфигурация

Приложение берёт все нужные параметры из конфигурационного файла `config.toml`.
Пример конфигурационного файла (`config.toml.example`):

```TOML
[scheduleBot]
token   = 'yourTelegramBotTokenHere'
    [scheduleBot.options]
    polling = true
    privacy = false

[mongoClient]
port       = '27017'
host       = '127.0.0.1'
collection = 'collectionName'

auth       = true
user       = 'mongoUser'
password   = 'mongoPassword'

```

_Подробнее про [TOML](https://github.com/toml-lang/toml)_

**scheduleBot**: - опции для основного приложения
- **token** - токен, выданный вашему боту от [@BotFather](http://telegram.me/BotFather)
- **scheduleBot.options** - подробнее в [документации node-telegram-bot-api](https://github.com/yagop/node-telegram-bot-api)

**mongoClient**: - опции для клиента MongoDB
- **port**       - порт, на котором слушает MongoDB
- **host**       - адрес хоста
- **collection** - имя коллекции, в которой будут хранится записи
- **auth**       - нужно ли производить авторизацию (**true** или **false**)
- **user**, **password**

Если нужно запустить приложение с другим конфигурационным файлом, то путь к нему
можно передать как первый аргумент. Подробнее в [Запуск]()

### Запуск

Входная точка приложения -  `index.js`. Для логирования используется
[bunyan](https://github.com/trentm/node-bunyan).

Запустить приложение можно командой:

```shell
$ node index.js | bunyan
```


Запуск с конфигурационным файлом, отличным от стандартного:

```shell
$ node index.js another_config.toml | bunyan
```

### Скриншоты
